<?php

require_once 'grafieken/phpgraphlib.php';
require_once 'db.php';


$data = getData();
$graph = new PHPGraphLib(650,200);
$graph->addData($data);
$graph->createGraph();

<?php

session_start();

require_once 'db.php';


if(isset($_GET['stad'])){

    //Verwijder "white space"
    $stad = preg_replace('/\s+/', '', $_GET['stad']);

    //json ophalen
    $json = file_get_contents('http://api.openweathermap.org/data/2.5/weather?q='.$stad.'&mode=JSON&units=metric&appid=a965ee8473256801c01383d2b282edc5');

    //json naar een array omzetten
    $data = json_decode($json, true);


    $city = array(
        'temp' => $data['main']['temp'],
        'city' => $data['name'],
        'lat'  => $data['coord']['lat'],
        'lng'  => $data['coord']['lon']
    );

    insert($city);


    //de temp van lon en lat in de url
    echo $data['main']['temp'];
}


 ?>

 <!DOCTYPE html>
 <html>
 <head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js" type="text/javascript"></script>

 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<title></title>
 	<link rel="stylesheet" href="">
 </head>
 <body>


    <form action="zoekPlaats.php" method="post">
        <input type="text" name="plaats" placeholder="Zoek een plaatsnaam">
        <input type="submit" value="Zoek!">
    </form>

 <?php

    if(isset($_SESSION['steden'])){

    echo '<ul>';

        foreach($_SESSION['steden']['predictions'] as $stad){
            echo '<li><a href="test.php?stad='.$stad['description']. '">'. $stad['description'] .'</a></li>';
        }

     echo '</ul>';

    }

 ?>


 </body>
 </html>